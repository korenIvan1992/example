package com.test.test.network;

import android.content.Context;

import com.google.gson.Gson;
import com.readystatesoftware.chuck.ChuckInterceptor;
import com.test.test.utils.ConstantsUtil;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
/**
 * Created by koren on 19.09.17.
 */
@Singleton
public class ServiceGenerator {

    private final static int TIMEOUT_CONNECT = 50; //in seconds
    private final static int TIMEOUT_READ = 50; //in seconds

    private OkHttpClient.Builder okBuilder;
    private Gson gson;

    @Inject
    Context mContext;

    @Inject
    ServiceGenerator(Gson gson, Context context) {
        this.mContext = context;
        this.okBuilder = new OkHttpClient.Builder();
        this.okBuilder.addInterceptor(headerInterceptor);
        this.okBuilder.connectTimeout(TIMEOUT_CONNECT, TimeUnit.SECONDS);
        this.okBuilder.addInterceptor(new ChuckInterceptor(mContext));
        this.okBuilder.readTimeout(TIMEOUT_READ, TimeUnit.SECONDS);
        this.okBuilder.cache(cache());
        this.okBuilder.addInterceptor(getLogInterceptor());
        this.gson = gson;
    }

    public <S> S createService(Class<S> serviceClass, String BASE_URL) {
        OkHttpClient client = okBuilder.build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return retrofit.create(serviceClass);
    }

    @SuppressWarnings("all")
    private Interceptor headerInterceptor = chain -> {
        Request original = chain.request();
        Request request = original.newBuilder()
                .header(ConstantsUtil.CONTENT_TYPE_CONSTANT, ConstantsUtil.CONTENT_TYPE)
                .header(ConstantsUtil.ACCEPT_CONSTANT, ConstantsUtil.ACCEPT)
                .method(original.method(), original.body())
                .build();
        return chain.proceed(request);
    };

    private HttpLoggingInterceptor getLogInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    private Cache cache() {
        int cacheSize = 10 * 1024 * 1024; // 10 MB
        return new Cache(mContext.getCacheDir(), cacheSize);
    }
}
