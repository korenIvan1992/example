package com.test.test.network.di;

import android.content.Context;

/**
 * Created by koren on 19.09.17.
 */
public class DaggerService {

    private MainComponent daggerMainComponent;

    public DaggerService(Context context) {
        daggerMainComponent = DaggerMainComponent.builder()
                .mainModule(new MainModule(context))
                .build();
    }

    public MainComponent getDaggerMainComponent() {
        return daggerMainComponent;
    }

}
