package com.test.test.network.di

import com.test.test.ui.components.main.MainActivity
import com.test.test.ui.components.spash.SplashActivity

import javax.inject.Singleton

import dagger.Component
/**
 * Created by koren on 19.09.17.
 */
@Singleton
@Component(modules = [MainModule::class])
interface MainComponent {

    fun inject(mainActivity: SplashActivity)

    fun inject(mainActivity: MainActivity)




}