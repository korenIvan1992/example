package com.test.test.network.di;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
/**
 * Created by koren on 19.09.17.
 */
@Module
public class ContextModule {

    public Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context context() {
        return context.getApplicationContext();
    }
}
