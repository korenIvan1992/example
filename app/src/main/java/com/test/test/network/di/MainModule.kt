package com.test.test.network.di

import android.content.Context
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton
/**
 * Created by koren on 19.09.17.
 */
@Module
class MainModule(@get:Provides
                 val context: Context) {

    @Singleton
    @Provides
    fun provideGson(): Gson {

        return Gson()
    }

    @Provides
    fun provideCompositeDisposable(): CompositeDisposable {
        return CompositeDisposable()
    }

}
