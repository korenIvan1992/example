package com.test.test.network

import android.content.Context
import com.google.gson.Gson
import com.test.test.data.remote.serviceDto.ServiceError
import com.test.test.data.remote.serviceDto.ServiceResponse
import com.test.test.utils.ConstantsUtil
import com.test.test.utils.L
import com.test.test.utils.NetworkUtils
import com.test.test.utils.ObjectUtil
import retrofit2.Call
import java.io.IOException
import javax.inject.Inject
/**
 * Created by koren on 19.09.17.
 */

class GatewayRepositories @Inject
internal constructor(val serviceGenerator: ServiceGenerator, private val gson: Gson, private val context: Context) {
    private val TAG = this.javaClass.simpleName

    /**
     * @param call request call data
     * @return ServiceResponse
     */
    fun processCall(call: Call<*>): ServiceResponse {
        //        if (isCheck)
        //            return null;

        if (!NetworkUtils.isConnected(context)) {
            L.e(TAG, NO_NETWORK)
            return ServiceResponse(ServiceError(NO_NETWORK, ConstantsUtil.ERROR_UNDEFINED))
        }

        try {
            val response = call.execute()
            if (ObjectUtil.isNull(response))
                return ServiceResponse(ServiceError("No response from server", ConstantsUtil.ERROR_UNDEFINED))

            // Выводит в лог тело запроса
            if (!ObjectUtil.isNull(response.body()))
                L.json(gson.toJson(response.body()))

            val responseCode = response.code()
            if (response.isSuccessful)
                return ServiceResponse(responseCode, response.body()!!)
            else {
                val serviceError: ServiceError
                val errorBody = response.errorBody()
                if (!ObjectUtil.isNull(errorBody)) {
                    serviceError = ServiceError(errorBody!!.string(), responseCode)
                } else
                    serviceError = ServiceError("Null response body from server", responseCode)
                return ServiceResponse(serviceError)
            }
        } catch (e: IOException) {
            //            L.ex(e);
            return ServiceResponse(ServiceError(e.toString(), ConstantsUtil.ERROR_UNDEFINED))
        }

    }

    companion object {

        const val NO_NETWORK = "No network connection"
    }

}
