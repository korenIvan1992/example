package com.test.test.ui.components.spash;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.test.test.MyApplication;
import com.test.test.R;
import com.test.test.ui.base.BaseActivity;
import com.test.test.ui.base.common.Layout;
import com.test.test.ui.components.main.MainActivity;
import com.test.test.utils.AlertUtil;

import javax.inject.Inject;

/**
 * Created by koren on 19.09.17.
 */

@Layout(id = R.layout.activity_splash)
public class SplashActivity extends BaseActivity implements SplashInterface.View {


    @Inject
    SplashPresenter mPresenter;

    @Override
    protected void initPresenter() {
        super.setPresenter(mPresenter);
        mPresenter.setView(this);
    }

    @Override
    protected void initDagger() {
        ((MyApplication) getApplicationContext())
                .getMainComponent()
                .inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkConnection();
    }


    @Override
    public void showNoConnectionDialog() {
        AlertUtil.showAlertConnection(this,
                onSettingsClickListener(), onOkClickListener());
    }

    public void showSettingsPage() {
        startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
    }

    @Override
    public void showLoginPage() {
        startActivity(new Intent(this, MainActivity.class));
        finish();

    }


    @Override
    public void setToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void checkConnection() {
        mPresenter.checkConnectionStatus(this);
    }

    private DialogInterface.OnClickListener onSettingsClickListener() {
        return (dialogInterface, i) -> {
            showSettingsPage();
            finish();
        };
    }

    private DialogInterface.OnClickListener onOkClickListener() {
        return (dialogInterface, i) -> checkConnection();
    }
}
