package com.test.test.ui.base

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.test.test.R
import com.test.test.ui.base.common.Layout

/**
 * Created by koren on 19.09.17.
 */
abstract class BaseActivity : AppCompatActivity(), BaseView {

    @Nullable
    @BindView(R.id.toolbar)
    lateinit var toolbar: Toolbar

    var allView: ViewGroup? = null

    private var TAG = this.javaClass.simpleName

    protected var presenter: Presenter<*>? = null

    private var unbinder: Unbinder? = null

    private val layoutResId: Int
        get() {
            val cls = javaClass
            if (!cls.isAnnotationPresent(Layout::class.java))
                throw RuntimeException("Layout for $TAG doesn't present")
            val annotation = cls.getAnnotation(Layout::class.java)
            val layout = annotation as Layout
            return layout.id
        }

    protected abstract fun initPresenter()

    protected abstract fun initDagger()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)
        unbinder = ButterKnife.bind(this)
        initDagger()
        initPresenter()

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        val id = item.itemId
//        if (id == android.R.id.home) {
//            finish()
//        }
//        return super.onOptionsItemSelected(item)
//    }

//    override fun setUpIconVisibility(visibility: Boolean) {
//        val actionBar = supportActionBar
//        actionBar?.setDisplayHomeAsUpEnabled(visibility)
//    }


    override fun onStart() {
        super.onStart()
        if (presenter != null)
            presenter!!.start()
    }

    override fun onStop() {
        super.onStop()
        if (presenter != null)
            presenter!!.finalizeView()
    }

    override fun onDestroy() {
        super.onDestroy()
        unbinder!!.unbind()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun setTitle(title: String) {
    }
}

