package com.test.test.ui.components.main;

import com.test.test.data.remote.models.main.Value;
import com.test.test.ui.base.BaseView;

import java.util.List;

/**
 * Created by koren on 19.09.17.
 */
public interface MainInterface {
    interface Presenter {
      void  getListValue();

        void remove();
    }

    interface View extends BaseView {
        void setAdapter(List<Value> list);
        void setToast(String text);

    }

}
