package com.test.test.ui.components.main;


import com.test.test.data.remote.models.main.MainResponseBody;
import com.test.test.data.usecases.main.MainUseCase;
import com.test.test.ui.base.Presenter;

import java.util.Objects;

import javax.inject.Inject;

/**
 * Created by koren on 19.09.17.
 */
public class MainPresenter extends Presenter<MainInterface.View> implements MainInterface.Presenter {

    private MainUseCase mainUseCase;

    @Inject
    public MainPresenter(MainUseCase mainUseCase) {
        this.mainUseCase = mainUseCase;
    }

    @Override
    public void getListValue() {
        mainUseCase.getListValueUse((b, text, responseMain) -> {
            MainResponseBody response = (MainResponseBody) responseMain;
            if (b) {
                getView().setAdapter(Objects.requireNonNull(response).getStock());
            } else
                getView().setToast(text);
        });
    }

    @Override
    public void remove() {
        mainUseCase.unSubscribe();
    }
}
