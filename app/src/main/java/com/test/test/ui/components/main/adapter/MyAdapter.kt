package com.test.test.ui.components.main.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.test.test.R
import com.test.test.data.remote.models.main.Value
import io.reactivex.annotations.NonNull

class MyAdapter(var list: MutableList<Value>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemViewType(position: Int): Int {
        val i = list.size - 1
        return if (i == position)
            2
        else
            1
    }

    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 1) {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_adapter,
                            parent, false)
            return MyViewHolder(view)
        } else {
            //для пагинации шаблон
            val view1 = LayoutInflater.from(parent.context)
                    .inflate(R.layout.pagination_layout, parent, false)
            return ViewHolderFooterView(view1)
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val i = holder.itemViewType
        if (i == 1) {
            val amountString: String = String.format("%.2f", list[position].price!!.amount)
            val myViewHolder = holder as MyViewHolder
            myViewHolder.name.text = list[position].name
            myViewHolder.volume.text = list[position].volume.toString()
            myViewHolder.amount.text = amountString
        } else {
            val footerView = holder as ViewHolderFooterView
            //  Если будет пагинация
            //  footerView.progressBar.visibility = View.VISIBLE
        }
    }


    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.name)
        lateinit var name: TextView
        @BindView(R.id.volume)
        lateinit var volume: TextView
        @BindView(R.id.amount)
        lateinit var amount: TextView

        init {
            ButterKnife.bind(this, itemView)
        }

    }

    class ViewHolderFooterView(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.progressBar)
        lateinit var progressBar: ProgressBar

        init {
            ButterKnife.bind(this, itemView)
        }
    }
}
