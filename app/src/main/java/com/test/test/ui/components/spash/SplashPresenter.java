package com.test.test.ui.components.spash;

import android.content.Context;

import com.test.test.ui.base.Presenter;
import com.test.test.utils.ConnectionUtil;

import javax.inject.Inject;
/**
 * Created by koren on 19.09.17.
 */
public class SplashPresenter extends Presenter<SplashInterface.View> implements SplashInterface.Presenter {

    @Inject
    public SplashPresenter() {
    }

    @Override
    public void checkConnectionStatus(Context context) {
        if (ConnectionUtil.getStatus(context))
            getView().showLoginPage();
        else
            getView().showNoConnectionDialog();
    }

}