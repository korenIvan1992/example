package com.test.test.ui.components.spash;

import android.content.Context;

import com.test.test.ui.base.BaseView;

/**
 * Created by koren on 19.09.17.
 */
public class SplashInterface {

    public interface Presenter  {
        void checkConnectionStatus(Context context);


    }

    public interface View extends BaseView {

        void showNoConnectionDialog();

        void showSettingsPage();

        void showLoginPage();

        void setToast(String text);
    }
}