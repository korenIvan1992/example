package com.test.test.ui.base.common

import android.support.annotation.LayoutRes
/**
 * Created by koren on 19.09.17.
 */
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
annotation class Layout(@LayoutRes val id: Int)
