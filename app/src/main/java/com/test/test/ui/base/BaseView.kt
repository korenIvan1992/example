package com.test.test.ui.base
/**
 * Created by koren on 19.09.17.
 */
interface BaseView {
    fun setTitle(title: String)
}
