package com.test.test.ui.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.text.TextUtils
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.test.test.R
import com.test.test.ui.base.common.Layout
import com.test.test.utils.L
/**
 * Created by koren on 19.09.17.
 */
abstract class BaseFragment : Fragment(), BaseView {

    val TAG = this.javaClass.simpleName

    internal var fragmentManager: FragmentManager?=null

    protected var presenter: Presenter<*>? = null

    internal var view: View? = null

    private var unbinder: Unbinder? = null

    private val toolbarTitleKey: String? = null

    private val layoutId: Int
        get() {
            val cls = javaClass
            if (!cls.isAnnotationPresent(Layout::class.java))
                throw RuntimeException("Layout for $TAG don't present")
            val annotation = cls.getAnnotation(Layout::class.java)
            val layout = annotation as Layout
            return layout.id
        }

    protected abstract fun initializeDagger()

    protected abstract fun initializePresenter()

    //    @Nullable
    //    @BindView(R.id.backButton)
    //    ImageView backButton;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        assert(activity != null)
        fragmentManager = activity!!.supportFragmentManager
        initializeDagger()
        initializePresenter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        view = inflater.inflate(layoutId, container, false)
        unbinder = ButterKnife.bind(this, view!!)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (presenter != null) {
            presenter!!.initialize(this.arguments)
        }
    }


    override fun onStart() {
        super.onStart()
        if (presenter != null) {
            presenter!!.start()
        }
    }

    override fun onStop() {
        super.onStop()
        if (presenter != null) {
            presenter!!.finalizeView()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder!!.unbind()
    }

    override fun setTitle(title: String) {

    }

}
