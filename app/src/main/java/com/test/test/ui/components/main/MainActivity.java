package com.test.test.ui.components.main;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.test.test.MyApplication;
import com.test.test.R;
import com.test.test.data.remote.models.main.Value;
import com.test.test.ui.base.BaseActivity;
import com.test.test.ui.base.common.Layout;
import com.test.test.ui.components.main.adapter.MyAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by koren on 19.09.17.
 */
@Layout(id = R.layout.activity_main)
public class MainActivity extends BaseActivity implements MainInterface.View {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.all_view)
    ConstraintLayout allView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Inject
    MainPresenter mPresenter;

    private Timer mTimer;
    private boolean checkTimer = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
        initUI();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.refresh));
    }

    @SuppressLint("ClickableViewAccessibility")
    public void initUI() {
        mTimer = new Timer();
    }

    @Override
    protected void initPresenter() {
        super.setPresenter(mPresenter);
        mPresenter.setView(this);
    }

    @Override
    protected void initDagger() {
        ((MyApplication) getApplicationContext())
                .getMainComponent()
                .inject(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.refresh) {
            progressBar.setVisibility(View.VISIBLE);
            mPresenter.getListValue();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setTitle(@NotNull String title) {

    }

    @Override
    public void setAdapter(List<Value> list) {
        TransitionManager.beginDelayedTransition(allView);
        allView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        final MyAdapter adapter = new MyAdapter(list);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(adapter);
        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView rv, int newState) {
                super.onScrollStateChanged(rv, newState);

            }

            @Override
            public void onScrolled(@NonNull RecyclerView rv, int dx, int dy) {
                super.onScrolled(recycler, dx, dy);
                LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recycler.getLayoutManager());
                int totalItemCount = layoutManager.getItemCount() - 1;
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                if (totalItemCount == lastVisible) {
                    // Если будет Пагинация - прописываем логику
                }
            }
        });

    }

    @Override
    public void setToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    private void startTimer() {
        if (checkTimer) {
            mTimer = new Timer();
            TimerTask mTask = new TimerTask() {
                public void run() {
                    mPresenter.getListValue();
                }
            };
            mTimer.schedule(mTask, 1, 15000);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        checkTimer = false;
        mTimer.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkTimer = true;
        startTimer();
    }

    @Override
    public void onBackPressed() {
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.remove();
    }
}
