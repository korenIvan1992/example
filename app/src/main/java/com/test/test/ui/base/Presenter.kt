package com.test.test.ui.base

import android.os.Bundle

import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicBoolean
/**
 * Created by koren on 19.09.17.
 */
abstract class Presenter<T : BaseView> {

    private var view: WeakReference<T>? = null

    private val isViewAlive = AtomicBoolean()

    fun getView(): T {
        return view!!.get()!!
    }

    fun setView(view: T) {
        this.view = WeakReference(view)
    }

    fun start() {
        isViewAlive.set(true)
    }

    fun finalizeView() {
        isViewAlive.set(false)
    }

    fun initialize(extras: Bundle?) {}
}
