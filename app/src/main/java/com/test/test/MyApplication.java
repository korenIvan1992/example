package com.test.test;

import android.annotation.SuppressLint;
import android.app.Application;

import com.test.test.network.di.DaggerService;
import com.test.test.network.di.MainComponent;

/**
 * Created by koren on 19.09.17.
 */
public class MyApplication extends Application {

    private MainComponent mainComponent;

    @SuppressLint("HardwareIds")
    @Override
    public void onCreate() {
        super.onCreate();
        mainComponent = new DaggerService(this).getDaggerMainComponent();
    }

    public MainComponent getMainComponent() {
        return mainComponent;
    }

}