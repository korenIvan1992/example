package com.test.test.data.usecases.main


import android.util.Log
import com.test.test.data.DataRepository
import com.test.test.data.remote.callbackInterface.IAnswerCallback
import com.test.test.data.remote.models.main.MainResponseBody
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by koren on 19.09.17.
 */


class MainUseCase @Inject
internal constructor(private val dataRepository: DataRepository, private val compositeDisposable: CompositeDisposable) : IMainUseCase {
    private var dis1: Disposable? = null

    override fun getListValueUse(callback: IAnswerCallback) {
        val dis = object : DisposableSingleObserver<MainResponseBody>() {
            override fun onSuccess(response: MainResponseBody) {
                Log.e(this.javaClass.simpleName, "seq")
                callback.answer(true, "", response)
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
                callback.answer(false, e.message, null)
            }
        }
        if (!compositeDisposable.isDisposed) {
            val single = dataRepository.getListValueData()
            dis1 = single.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith<DisposableSingleObserver<MainResponseBody>>(dis)
            compositeDisposable.add(dis1!!)
        }
    }

    fun unSubscribe() {
        if (!compositeDisposable.isDisposed) {
            if (dis1 != null) {
                compositeDisposable.remove(dis1!!)
            }
        }
    }
}
