package com.test.test.data

import com.test.test.data.remote.models.main.MainResponseBody
import io.reactivex.Single
/**
 * Created by koren on 19.09.17.
 */

interface DataSource {

    fun getListValueData(): Single<MainResponseBody>

}
