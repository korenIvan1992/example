package com.test.test.data.remote.callbackInterface
/**
 * Created by koren on 19.09.17.
 */
interface IObjectCallback {
    fun callBack(b: Boolean, text: String?, responseConfig: Any?)
}
