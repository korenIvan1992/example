package com.test.test.data.remote.services

import com.test.test.data.remote.models.main.MainResponseBody
import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by koren on 19.09.17.
 */

interface MainService {

    @GET("stocks.json")
    fun getValue(): Call<MainResponseBody>

}
