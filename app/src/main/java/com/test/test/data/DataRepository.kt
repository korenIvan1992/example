package com.test.test.data


import com.test.test.data.local.LocalRepository
import com.test.test.data.remote.models.main.MainResponseBody
import com.test.test.data.remote.repositories.RemoteRepositoriesInterLayer
import io.reactivex.Single
import javax.inject.Inject
/**
 * Created by koren on 19.09.17.
 */

class DataRepository @Inject
internal constructor(private val localRepository: LocalRepository, private val remoteRepos: RemoteRepositoriesInterLayer) : DataSource {


    override fun getListValueData(): Single<MainResponseBody> {
        return remoteRepos.jobEntryRepositories.requestListValue()
    }

}
