package com.test.test.data.usecases.main

import com.test.test.data.remote.callbackInterface.IAnswerCallback

/**
 * Created by koren on 19.09.17.
 */

interface IMainUseCase {

    fun getListValueUse(callback: IAnswerCallback)

}
