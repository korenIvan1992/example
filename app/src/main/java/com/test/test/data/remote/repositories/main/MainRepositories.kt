package com.test.test.data.remote.repositories.main

import android.accounts.NetworkErrorException
import android.content.Context
import com.test.test.data.remote.models.main.MainResponseBody

import com.test.test.data.remote.serviceDto.ServiceError
import com.test.test.data.remote.services.MainService
import com.test.test.network.GatewayRepositories

import com.test.test.utils.ConstantsUtil
import com.test.test.utils.NetworkUtils

import javax.inject.Inject

import io.reactivex.Single


/**
 * Created by koren on 19.09.17.
 */
class MainRepositories @Inject
internal constructor(private val gatewayRepositories: GatewayRepositories, private val mContext: Context) : IMainRepository {

    override fun requestListValue(): Single<MainResponseBody> {
        //отправляем id
        //        RxJavaPlugins.setErrorHandler(Crashlytics::logException);
        return Single.create { emitter ->
            if (!NetworkUtils.isConnected(mContext)) {
                val exception = Exception(GatewayRepositories.NO_NETWORK)
                emitter.onError(exception)
            } else {
                val service = gatewayRepositories.serviceGenerator.createService(MainService::class.java, ConstantsUtil.BASE_URL)
                val serviceResponse = gatewayRepositories.processCall(service.getValue())

                if (ServiceError.isSuccess(serviceResponse.code)) {
                    val response = serviceResponse.data as MainResponseBody
                    emitter.onSuccess(response)
                } else {
                    val throwable = NetworkErrorException(serviceResponse.serviceError!!.description)
                    emitter.onError(throwable)
                }
            }
        }
    }
}
