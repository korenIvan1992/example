package com.test.test.data.remote.callbackInterface

/**
 * Created by koren on 19.09.17.
 */
interface IAnswerCallback {
    fun answer(s: Boolean, text: String?,response: Any?)
}
