package com.test.test.data.remote.models.main

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Value {

    @Expose
    @SerializedName("name")
    var name: String? = null

    @Expose
    @SerializedName("price")
    var price: Price? = null

    @Expose
    @SerializedName("percent_change")
    var percentChange: Double? = null


    @Expose
    @SerializedName("volume")
    var volume: Int? = null

    @Expose
    @SerializedName("symbol")
    var symbol: String? = null
}

