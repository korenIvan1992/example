package com.test.test.data.remote.repositories.main

import io.reactivex.Single

/**
 * Created by koren on 19.09.17.
 */
interface IMainRepository {

    fun requestListValue(): Single<*>

}
