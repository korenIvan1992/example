package com.test.test.data.remote.serviceDto
/**
 * Created by koren on 19.09.17.
 */

class ServiceResponse {

    var code: Int = 0
    var data: Any? = null
    var serviceError: ServiceError? = null

    constructor() {}

    constructor(code: Int, data: Any) {
        this.code = code
        this.data = data
    }

    constructor(code: Int, data: Any, serviceError: ServiceError) {
        this.code = code
        this.data = data
        this.serviceError = serviceError
    }

    constructor(serviceError: ServiceError) {
        this.serviceError = serviceError
    }
}
