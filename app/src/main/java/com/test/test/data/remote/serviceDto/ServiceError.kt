package com.test.test.data.remote.serviceDto

/**
 * Created by koren on 19.09.17.
 */

class ServiceError {
    var description: String? = null
    var code: Int? = null

    constructor() {}

    constructor(description: String, code: Int) {
        this.description = description
        this.code = code
    }

    companion object {
        val NETWORK_ERROR = "Unknown network service error"
        private val GROUP_200 = 2
        private val GROUP_400 = 4
        private val GROUP_500 = 5
        private val VALUE_100 = 100
        val SUCCESS_CODE = 200
        val ERROR_CODE = 400

        fun isSuccess(responseCode: Int): Boolean {
            return responseCode / VALUE_100 == GROUP_200
        }

        fun isClientError(errorCode: Int): Boolean {
            return errorCode / VALUE_100 == GROUP_400
        }

        fun isServerError(errorCode: Int): Boolean {
            return errorCode / VALUE_100 == GROUP_500
        }

//        fun isErrorMessage(`object`: Any): Boolean {
//            val messagesError = `object` as MessagesError
//            return messagesError != null
//        }
    }
}
