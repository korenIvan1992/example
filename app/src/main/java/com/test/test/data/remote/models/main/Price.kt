package com.test.test.data.remote.models.main

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class Price {

    @Expose
    @SerializedName("currency")
    var currency: String? = null
    @Expose
    @SerializedName("amount")
    var amount: Double? = null

}
