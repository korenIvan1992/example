package com.test.test.data.remote.models.main

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by koren on 19.09.17.
 */
class MainResponseBody {

    @Expose
    @SerializedName("stock")
    var stock: List<Value>? = null
}
