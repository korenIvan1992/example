package com.test.test.data.remote.repositories

import com.test.test.data.remote.repositories.main.MainRepositories

import javax.inject.Inject
/**
 * Created by koren on 19.09.17.
 */
class RemoteRepositoriesInterLayer @Inject
constructor(val jobEntryRepositories: MainRepositories)
