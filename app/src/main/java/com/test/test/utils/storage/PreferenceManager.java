package com.test.test.utils.storage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

/**
 * Created by koren on 19.09.17.
 */

@SuppressLint("CommitPrefEdits")
public class PreferenceManager {

    private static final String PREF_NAME = "PreferenceManager.TEST";



    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Inject
    PreferenceManager(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, 0);
        editor = pref.edit();
    }

}
