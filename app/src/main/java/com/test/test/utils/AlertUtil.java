package com.test.test.utils;

import android.content.Context;
import android.content.DialogInterface;

import com.test.test.R;

/**
 * Created by koren on 19.09.17.
 */
public class AlertUtil {

    public static void showAlertConnection(Context context,
                                           DialogInterface.OnClickListener negativeListener,
                                           DialogInterface.OnClickListener positiveListener) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setTitle(R.string.check_internet)
                .setNegativeButton(R.string.setting, negativeListener)
                .setPositiveButton(R.string.ok_text, positiveListener)
                .setCancelable(false)
                .create();
        builder.show();
    }
}
