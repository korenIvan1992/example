package com.test.test.utils;
/**
 * Created by koren on 19.09.17.
 */
public class ConstantsUtil {
  public static String BASE_URL = "http://phisix-api3.appspot.com/";
  public static final String CONTENT_TYPE = "application/json";
  public static final String ACCEPT = "application/json";
  public static int ERROR_UNDEFINED = -1;
  public static final String CONTENT_TYPE_CONSTANT = "Content-Type";
  public static final String ACCEPT_CONSTANT = "Accept";

}
