package com.test.test.utils;
/**
 * Created by koren on 19.09.17.
 */
public class ObjectUtil {

    public static boolean isNull(Object obj) {
        return obj == null;
    }

    public static boolean isEmpty(String str) {
        return str == null || str.isEmpty();
    }
}
