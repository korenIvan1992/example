package com.test.test.utils;

import android.util.Log;

import com.test.test.BuildConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * Created by koren on 19.09.17.
 */

public class L {

    private static String DEBUG = "DEBUG";
    private static int JSON_INDENT = 2;

    public static void e(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, msg);
        }
    }

    public static void d(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, msg);
        }
    }


    public static void i(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.i(tag, msg);
        }
    }

    public static void v(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.v(tag, msg);
        }
    }

//    public static void ex(Throwable throwable) {
//        if (BuildConfig.DEBUG)
//            throwable.printStackTrace();
//        else if (Fabric.isInitialized()) {
//            Crashlytics.logException(throwable);
//        } else
//            Log.e("Throwable", throwable.toString(), throwable);
//    }

    public static void json(String json) {
        if (ObjectUtil.isEmpty(json)) {
            d(DEBUG, "Empty/Null json content");
            return;
        }
        try {
            json = json.trim();
            if (json.startsWith("{")) {
                JSONObject jsonObject = new JSONObject(json);
                String message = jsonObject.toString(JSON_INDENT);
                d(DEBUG, message);
                return;
            }
            if (json.startsWith("[")) {
                JSONArray jsonArray = new JSONArray(json);
                String message = jsonArray.toString(JSON_INDENT);
                d(DEBUG, message);
                return;
            }
            e(DEBUG, "Invalid Json");
        } catch (JSONException e) {
//            ex(e);
        }
    }
}
